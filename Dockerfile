FROM timberio/vector:0.26.0-debian
COPY vector.toml .
RUN /usr/bin/apt update && /usr/bin/apt install -y nano htop
#COPY ./start-fly-stats-transporter.sh .
# CMD ["bash", "start-fly-stats-transporter.sh"]
# ENTRYPOINT []
CMD ["-w","-c","vector.toml" ]
