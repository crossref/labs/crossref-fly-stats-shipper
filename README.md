## What is this?

This is a headless app runing [Vector](https://vector.dev/) to collect and ship stats.

It is currently configured to do two things:

- Accept [statsd](https://github.com/statsd/statsd)-ish metrics and then forward them to our hosted Prometheus instance.  
- Scrape our various labs /metrics endpoints and forward them to our hosted prometheus instance.

## Accepting and forwarding statsd metrics

This is useful if you cannot easily instrument your app with a prometheus `/metrics` end-point and you don't want to depend on structured-logging to [Loki](https://grafana.com/oss/loki/) for gathering your metrics.

SPecifically, this is useful if you are  building a prototype using [Streamlit](https://streamlit.io/) or any [Tornado](https://www.tornadoweb.org/en/stable/)-based server.

To use this within the Crossref org on fly.io, you need to use TCP to send statsd messages to the following vector_host:

```bash
crossref-fly-stats-shipper.internal
```

So, for example, to send a counter event for the app, APP_NAME,  with the labels "`method` and `member` in Python you would:

```bash
pip install statsd-tags
```

Make sure you use 'statsd-tags' as normal statsd does not support labels (aka tags)

Then:

```python
import sys
import statsd
APP_NAME="my_app_name"
if not vector_host:= os.environ.get("VECTOR_HOST", None):
    sys.exit("VECTOR_HOST not set")

c = statsd.TCPStatsClient(host=vector_host, port=8125, ipv6=True)
c.incr(APP_NAME, tags={"method": "splat", "member": 98})
```

## Forwarding and consolidating Prometheus metrics

This is often done using Prometheus itself to scrape and forward stats, but Vector allows us to consolidate a lot of this functionality and is a bit easier to deploy because Vector supports niceties like ENV variables in config files (Prometheus devs seem steadfast in not supporting this basic functionality)

## How to run

To run this on `fly.io`, you need to set the following secrets:

- `PROMETHEUS_URL` (lookup in our password manager)
- `PROMETHEUS_USERNAME` (lookup in our password manager)
- `PROMETHEUS_PASSWORD` (lookup in our password manager)

